/*-
 * Copyright (c)2015 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if !defined(lint) && !defined(SHELL)
__COPYRIGHT("@(#) Copyright (c) 2015\
 Takehiko NOZAKI, All rights reserved.");
#endif /* not lint */

#include <err.h>
#include <getopt.h>
#include <limits.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *shortopts = "cCfghMmnRruVz";
static const struct option longopts[] =
{
	{ "check",                optional_argument, NULL, UCHAR_MAX+1 },
	{ "ignore-case",          no_argument,       NULL, 'f'         },
	{ "reverse",              no_argument,       NULL, 'r'         },
	{ "unique",               no_argument,       NULL, 'u'         },
	{ "zero-terminated",      no_argument,       NULL, 'z'         },
	{ "sort",                 required_argument, NULL, UCHAR_MAX+2 },
	{ "general-numeric-sort", no_argument,       NULL, 'g'         },
	{ "human-numeric-sort",   no_argument,       NULL, 'h'         },
	{ "month-sort",           no_argument,       NULL, 'M'         },
	{ "numeric-sort",         no_argument,       NULL, 'n'         },
	{ "random-sort",          no_argument,       NULL, 'R'         },
	{ "version-sort",         no_argument,       NULL, 'V'         },
	{ "merge",                no_argument,       NULL, 'm'         },
	{ NULL,                   0,                 NULL, 0           }
};

struct subopt {
	const char *name;
	int mode;
};

static const struct subopt checks[] = {
	{ "diagnose-first", 'c' },
	{ "quiet",          'C' },
	{ "silent",         'C' },
	{ NULL,              0  }
};

static const struct subopt sorts[] = {
	{ "general-numeric", 'g' },
	{ "human-numeric",   'h' },
	{ "month",           'M' },
	{ "numeric",         'n' },
	{ "random",          'R' },
	{ "version",         'V' },
	{ NULL,               0  }
};

struct line {
	char *buf;
	size_t bufsiz;
	ssize_t buflen;
};

struct config {
	FILE *reader;
	char delim;
	int mode;
	int argc;
	char **argv;
#define SRM_UNIQUE	0x1
#define SRM_SUPPRESS	0x2
#define SRM_MERGE	0x4
	int (*readfn)(struct config *, struct line *);
	int (*cmpfn)(struct config *, struct line *, struct line *);
	void (*sortfn)(struct config *);
};

static inline void
line_init(struct line *l)
{
	l->buf = NULL;
}

static inline void
line_uninit(struct line *l)
{
	free(l->buf);
}

static inline int
line_read(struct config *cf, struct line *l)
{
	l->buflen = getdelim(&l->buf, &l->bufsiz,
	    (int)cf->delim, cf->reader);
	if (l->buflen == -1) {
		if (ferror(cf->reader))
			err(EXIT_FAILURE, "getline");
		return 1;
	}
	return 0;
}

static int
line_strcmp(struct config *cf, struct line *a, struct line *b)
{
	return strcmp(a->buf, b->buf);
}

static int
line_strcmp_reverse(struct config *cf, struct line *a, struct line *b)
{
        return strcmp(b->buf, a->buf);
}

static int
line_strcasecmp(struct config *cf, struct line *a, struct line *b)
{
	return strcasecmp(a->buf, b->buf);
}

static int
line_strcasecmp_reverse(struct config *cf, struct line *a, struct line *b)
{
	return strcasecmp(b->buf, a->buf);
}

static inline void
line_swap(struct line *a, struct line *b)
{
	struct line c;

	c = *a;
	*a = *b;
	*b = c;
}

static inline int
parse_subopt(const struct subopt *head,
    const char *option, const char *s, int *mode)
{
	size_t n;
	const struct subopt *p;

	if (s == NULL) {
		*mode = head->mode;
		return 0;
	}
	n = strlen(s);
	for (p = head; p->name != NULL; ++p) {
		if (!strncasecmp(s, p->name, n)) {
			*mode = p->mode;
			return 0;
		}
	}
	warnx("invalid argument for '--%s': %s",
	    option, s);
	return 1;
}

static FILE *
file_open(const char *name, const char *mode)
{
        FILE *fp;

        fp = fopen(name, mode);
        if (fp == NULL)
                err(2, "fopen: %s", name);
        return fp;
}

static void
file_close(FILE *fp)
{
	if (fclose(fp) == EOF)
		errx(2, "fclose");
}

static void
usage()
{
	fprintf(stderr,
"usage: %1$s [-ghMnRV] [-fruz] [file]\n"
"       %1$s [-cC] [-fruz] [file]\n",
	    getprogname());
	exit(1);
}

static void
check_input(struct config *cf)
{
	struct line prev, cur;
	int ret;

	if (cf->argc > 1) {
		warnx("too many input files for '-cC' option");
		exit(2);
	}
	if (strcmp(cf->argv[0], "-"))
		cf->reader = file_open(cf->argv[0], "r");
	line_init(&prev);
	if ((*cf->readfn)(cf, &prev) == 0) {
                line_init(&cur);
		while ((*cf->readfn)(cf, &cur) == 0) {
			ret = (*cf->cmpfn)(cf, &cur, &prev);
			if (ret < 0) {
				if (cf->mode & SRM_SUPPRESS)
					exit(1);
				errx(1, "found disorder");
			}
			if ((cf->mode & SRM_UNIQUE) && !ret) {
				if (cf->mode & SRM_SUPPRESS)
					exit(1);
				errx(1, "found non-uniqueness");
			}
			line_swap(&prev, &cur);
		}
		line_uninit(&cur);
	}
	line_uninit(&prev);
}

static void
sort_default(struct config *cf)
{
}

static void
sort_general_numeric(struct config *cf)
{
}

static void
sort_human_numeric(struct config *cf)
{
}

static void
sort_month(struct config *cf)
{
}

static void
sort_numeric(struct config *cf)
{
}

static void
sort_random(struct config *cf)
{
}

static void
sort_version(struct config *cf)
{
}

static inline int
config_init(struct config *cf, int argc, char **argv)
{
	int ch, idx, check, sort, cmp;
#define CMP_IGNORECASE	0x1
#define CMP_REVERSE	0x2

	cf->reader = stdin;
	cf->delim = '\n';
	cf->mode = 0;
	cf->readfn = &line_read;

	check = 0;
	sort = 0;
	cmp = 0;

	while ((ch = getopt_long(argc, argv,
	    shortopts, longopts, &idx)) != -1) {
		switch (ch) {
		case UCHAR_MAX+1:
			if (parse_subopt(&checks[0],
			    longopts[idx].name, optarg, &ch))
				return 1;
		/*FALLTHROUGH*/
		case 'c':
		case 'C':
			if (check && check != ch) {
				warnx("option -cC are incompatible");
				return 1;
			}
			check = ch;
			break;
		case UCHAR_MAX+2:
			if (parse_subopt(&checks[0],
			    longopts[idx].name, optarg, &ch))
				return 1;
		/*FALLTHROUGH*/
		case 'g':
		case 'h':
		case 'M':
		case 'n':
		case 'R':
		case 'V':
			if (sort) {
				warnx("option -ghMnRV are incompatible");
				return 1;
			}
			sort = ch;
			break;
		case 'f':
			cmp |= CMP_IGNORECASE;
			break;
		case 'r':
			cmp |= CMP_REVERSE;
			break;
		case 'u':
			cf->mode |= SRM_UNIQUE;
			break;
		case 'z':
			cf->delim = '\0';
			break;
		case 'm':
			cf->mode |= SRM_MERGE;
			break;
		case '?':
		default:
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	cf->argc = argc;
	cf->argv = argv;

	if (cmp & CMP_IGNORECASE) {
		cf->cmpfn = (cmp & CMP_REVERSE)
		    ? &line_strcasecmp_reverse : &line_strcasecmp;
	} else {
		cf->cmpfn = (cmp & CMP_REVERSE)
		    ? &line_strcmp_reverse : &line_strcmp;
	}
	switch (check) {
	case 0:
		switch (sort) {
		case 0:
			cf->sortfn = &sort_default;
			break;
		case 'g':
			cf->sortfn = &sort_general_numeric;
			break;
		case 'h':
			cf->sortfn = &sort_human_numeric;
			break;
		case 'M':
			cf->sortfn = &sort_month;
			break;
		case 'n':
			cf->sortfn = &sort_numeric;
			break;
		case 'R':
			cf->sortfn = &sort_random;
			break;
		case 'V':
			cf->sortfn = &sort_version;
			break;
		}
		break;
	case 'C':
		cf->mode |= SRM_SUPPRESS;
	/*FALLTHROUGH*/
	case 'c':
		if (sort) {
			warnx("option -cC and -ghMnRV are incompatible");
			return 1;
		}
		cf->sortfn = &check_input;
		break;
	}
	return 0;
}

int
main(int argc, char *argv[])
{
	char **copy_argv;
	struct config cf;

	setprogname(argv[0]);

	setlocale(LC_ALL, "");

	if (config_init(&cf, argc, argv))
		usage();
	(*cf.sortfn)(&cf);

	exit(EXIT_SUCCESS);
}
