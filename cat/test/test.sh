#!/bin/sh -x
n=0
while read i; do
	prefix=`printf %02d $n`
	option=""
	suffix=""
	if [ -n "$i" ]; then
		option="-$i"
		suffix="_$i"
	fi
	../obj.amd64/cat $option test.txt >${prefix}_result${suffix}.tmp
	cmp ${prefix}_result${suffix}.tmp ${prefix}_result${suffix}.txt
	if [ $? -ne 0 ]; then
		echo "error"
		exit 1
	fi
	rm -f ${prefix}_result${suffix}.tmp
	n=`expr $n + 1`
done < options
