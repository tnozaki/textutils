/*-
 * Copyright (c)2021, 2022 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if !defined(lint) && !defined(SHELL)
__COPYRIGHT("@(#) Copyright (c) 2021, 2022\
 Takehiko NOZAKI, All rights reserved.");
#endif /* not lint */

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define S	(0x1)
#define N	(0x2)
#define B	(0x4)
#define E	(0x8)
#define T	(0x10)
#define V	(0x20)
#define F	(0x40)

static const char *optstr = "AbEeflnsTtuv";
static const struct option options[] = {
	{ "number-nonblank",  no_argument, NULL, 'b' },
	{ "number",           no_argument, NULL, 'n' },
	{ "squeeze-blank",    no_argument, NULL, 's' },
	{ "show-nonprinting", no_argument, NULL, 'v' },
	{ "show-all",         no_argument, NULL, 'A' },
	{ "show-ends",        no_argument, NULL, 'E' },
	{ "show-tabs",        no_argument, NULL, 'T' },
	{ NULL,               0,           NULL,  0  },
};

static char *inbuf, *outbuf, *ohead, *otail;
static ssize_t inbufsiz, outbufsiz;
static int prev, skip;
static uint32_t lnum;

typedef int (*t_catfn)(int);

#define ALWAYS_INLINE inline __attribute__((always_inline))

static ALWAYS_INLINE int
write_std(char *head, char *tail)
{
	ssize_t len;

	while (head < tail) {
		len = write(STDOUT_FILENO, head, tail - head);
		if (len < 0)
			return 1;
		head += len;
	}
	return 0;
}

static ALWAYS_INLINE int
write_char(int c)
{
	*ohead++ = c;
	if (ohead == otail) {
		if (write_std(outbuf, otail))
			return 1;
		ohead = outbuf;
	}
	return 0;
}

static ALWAYS_INLINE int
print_newline_std(void)
{
	return write_char('\n');
}

static ALWAYS_INLINE int
print_newline_e(void)
{
	return write_char('$') || write_char('\n');
}

static ALWAYS_INLINE int
print_tab_std(void)
{
	return write_char('\t');
}

static ALWAYS_INLINE int
print_tab_t(void)
{
	return write_char('^') || write_char('I');
}

static ALWAYS_INLINE int
print_char_std(int ch)
{
	return write_char(ch);
}

static ALWAYS_INLINE int
print_char_v(int ch)
{
	if (ch & 0x80) {
		if (write_char('M') || write_char('-'))
			return 1;
		ch &= 0x7f;
	}
	if (ch >= 0x0 && ch <= 0x1f) {
		if (write_char('^'))
			return 1;
		ch |= 0x40;
	} else if (ch == 0x7f) {
		if (write_char('^'))
			return 1;
		ch = '?';
	}
	return write_char(ch);
}

static int
print_number()
{
	++lnum;
	if (lnum < 10) {
		return write_char(' ')
		    || write_char(' ')
		    || write_char(' ')
		    || write_char(' ')
		    || write_char(' ')
		    || write_char('0' + lnum)
		    || write_char('\t');
	} else if (lnum < 100) {
		return write_char(' ')
		    || write_char(' ')
		    || write_char(' ')
		    || write_char(' ')
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 1000) {
		return write_char(' ')
		    || write_char(' ')
		    || write_char(' ')
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 10000) {
		return write_char(' ')
		    || write_char(' ')
		    || write_char('0' + (lnum / 1000) % 10)
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 100000) {
		return write_char(' ')
		    || write_char('0' + (lnum / 10000) % 10)
		    || write_char('0' + (lnum / 1000) % 10)
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 1000000) {
		return write_char('0' + (lnum / 100000) % 10)
		    || write_char('0' + (lnum / 10000) % 10)
		    || write_char('0' + (lnum / 1000) % 10)
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 10000000) {
		return write_char('0' + (lnum / 1000000) % 10)
		    || write_char('0' + (lnum / 100000) % 10)
		    || write_char('0' + (lnum / 10000) % 10)
		    || write_char('0' + (lnum / 1000) % 10)
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 100000000) {
		return write_char('0' + (lnum / 10000000) % 10)
		    || write_char('0' + (lnum / 1000000) % 10)
		    || write_char('0' + (lnum / 100000) % 10)
		    || write_char('0' + (lnum / 10000) % 10)
		    || write_char('0' + (lnum / 1000) % 10)
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	} else if (lnum < 1000000000) {
		return write_char('0' + (lnum / 100000000) % 10)
		    || write_char('0' + (lnum / 10000000) % 10)
		    || write_char('0' + (lnum / 1000000) % 10)
		    || write_char('0' + (lnum / 100000) % 10)
		    || write_char('0' + (lnum / 10000) % 10)
		    || write_char('0' + (lnum / 1000) % 10)
		    || write_char('0' + (lnum / 100) % 10)
		    || write_char('0' + (lnum / 10) % 10)
		    || write_char('0' + lnum % 10)
		    || write_char('\t');
	}
	return write_char('0' + (lnum / 1000000000) % 10)
	    || write_char('0' + (lnum / 100000000) % 10)
	    || write_char('0' + (lnum / 10000000) % 10)
	    || write_char('0' + (lnum / 1000000) % 10)
	    || write_char('0' + (lnum / 100000) % 10)
	    || write_char('0' + (lnum / 10000) % 10)
	    || write_char('0' + (lnum / 1000) % 10)
	    || write_char('0' + (lnum / 100) % 10)
	    || write_char('0' + (lnum / 10) % 10)
	    || write_char('0' + lnum % 10)
	    || write_char('\t');
}

static ALWAYS_INLINE int
print_number_std(int newline)
{
	return print_number();
}

static ALWAYS_INLINE int
print_number_b(int newline)
{
	return !newline && print_number();
}

static ALWAYS_INLINE int
print_number_be(int newline)
{
	if (!newline)
		return print_number();
	return write_char(' ')
	    || write_char(' ')
	    || write_char(' ')
	    || write_char(' ')
	    || write_char(' ')
	    || write_char(' ')
	    || write_char('\t');
}

#define WRITE(name, e, t, c)					\
static ALWAYS_INLINE int					\
write_##name(char *head, char *tail)				\
{								\
	int ch;							\
								\
	while (head < tail) {					\
		ch = (unsigned char)*head++;			\
		switch (ch) {					\
		case '\n':					\
			if (print_newline_##e())		\
				return 1;			\
			break;					\
		case '\t':					\
			if (print_tab_##t())			\
				return 1;			\
			break;					\
		default:					\
			if (print_char_##c(ch))			\
				return 1;			\
		}						\
	}							\
	return 0;						\
}
WRITE(e, e, std, std)
WRITE(t, std, t, std)
WRITE(v, std, std, v)
WRITE(et, e, t, std)
WRITE(ev, e, std, v)
WRITE(tv, std, t, v)
WRITE(etv, e, t, v)

#define WRITE_S(name, e, t, c)					\
static ALWAYS_INLINE int					\
write_##name(char *head, char *tail)				\
{								\
	int ch;							\
								\
	while (head < tail) {					\
		ch = (unsigned char)*head++;			\
		if (prev == '\n') {				\
			if (ch == '\n' && skip)			\
				continue;			\
			skip = ch == '\n';			\
		}						\
		switch (ch) {					\
		case '\n':					\
			if (print_newline_##e())		\
				return 1;			\
			break;					\
		case '\t':					\
			if (print_tab_##t())			\
				return 1;			\
			break;					\
		default:					\
			if (print_char_##c(ch))			\
				return 1;			\
		}						\
		prev = ch;					\
	}							\
	return 0;						\
}
WRITE_S(s, std, std, std)
WRITE_S(se, e, std, std)
WRITE_S(st, std, t, std)
WRITE_S(sv, std, std, v)
WRITE_S(set, e, t, std)
WRITE_S(sev, e, std, v)
WRITE_S(stv, std, t, v)
WRITE_S(setv, e, t, v)

#define WRITE_NB(name, e, t, c, n)				\
static ALWAYS_INLINE int					\
write_##name(char *head, char *tail)				\
{								\
	int ch;							\
								\
	while (head < tail) {					\
		ch = (unsigned char)*head++;			\
		if (prev == '\n') {				\
			if (print_number_##n(ch == '\n'))	\
				return 1;			\
		}						\
		switch (ch) {					\
		case '\n':					\
			if (print_newline_##e())		\
				return 1;			\
			break;					\
		case '\t':					\
			if (print_tab_##t())			\
				return 1;			\
			break;					\
		default:					\
			if (print_char_##c(ch))			\
				return 1;			\
		}						\
		prev = ch;					\
	}							\
	return 0;						\
}
WRITE_NB(n, std, std, std, std)
WRITE_NB(ne, e, std, std, std)
WRITE_NB(nt, std, t, std, std)
WRITE_NB(nv, std, std, v, std)
WRITE_NB(net, e, t, std, std)
WRITE_NB(nev, e, std, v, std)
WRITE_NB(ntv, std, t, v, std)
WRITE_NB(netv, e, t, v, std)
WRITE_NB(b, std, std, std, b)
WRITE_NB(be, e, std, std, be)
WRITE_NB(bt, std, t, std, b)
WRITE_NB(bv, std, std, v, b)
WRITE_NB(bet, e, t, std, be)
WRITE_NB(bev, e, std, v, be)
WRITE_NB(btv, std, t, v, b)
WRITE_NB(betv, e, t, v, be)

#define WRITE_SNB(name, e, t, c, n)				\
static ALWAYS_INLINE int					\
write_##name(char *head, char *tail)				\
{								\
	int ch;							\
								\
	while (head < tail) {					\
		ch = (unsigned char)*head++;			\
		if (prev == '\n') {				\
			if (ch == '\n' && skip)			\
				continue;			\
			skip = ch == '\n';			\
			if (print_number_##n(skip))		\
				return 1;			\
		}						\
		switch (ch) {					\
		case '\n':					\
			if (print_newline_##e())		\
				return 1;			\
			break;					\
		case '\t':					\
			if (print_tab_##t())			\
				return 1;			\
			break;					\
		default:					\
			if (print_char_##c(ch))			\
				return 1;			\
		}						\
		prev = ch;					\
	}							\
	return 0;						\
}
WRITE_SNB(sn, std, std, std, std)
WRITE_SNB(sne, e, std, std, std)
WRITE_SNB(snt, std, t, std, std)
WRITE_SNB(snv, std, std, v, std)
WRITE_SNB(snet, e, t, std, std)
WRITE_SNB(snev, e, std, v, std)
WRITE_SNB(sntv, std, t, v, std)
WRITE_SNB(snetv, e, t, v, std)
WRITE_SNB(sb, std, std, std, b)
WRITE_SNB(sbe, e, std, std, be)
WRITE_SNB(sbt, std, t, std, b)
WRITE_SNB(sbv, std, std, v, b)
WRITE_SNB(sbet, e, t, std, be)
WRITE_SNB(sbev, e, std, v, be)
WRITE_SNB(sbtv, std, t, v, b)
WRITE_SNB(sbetv, e, t, v, be)

#define CATFN(name)						\
static int							\
catfn_##name(int fd)						\
{								\
	ssize_t len;						\
								\
	while ((len = read(fd, inbuf, inbufsiz)) > 0) {		\
		if (write_##name(inbuf, inbuf + len))		\
			return 1;				\
	}							\
	if (len < 0)						\
		return 2;					\
	return 0;						\
}
CATFN(std)
CATFN(e)
CATFN(t)
CATFN(v)
CATFN(et)
CATFN(ev)
CATFN(tv)
CATFN(etv)
CATFN(s)
CATFN(se)
CATFN(st)
CATFN(sv)
CATFN(set)
CATFN(sev)
CATFN(stv)
CATFN(setv)
CATFN(n)
CATFN(ne)
CATFN(nt)
CATFN(nv)
CATFN(net)
CATFN(nev)
CATFN(ntv)
CATFN(netv)
CATFN(b)
CATFN(be)
CATFN(bt)
CATFN(bv)
CATFN(bet)
CATFN(bev)
CATFN(btv)
CATFN(betv)
CATFN(sn)
CATFN(sne)
CATFN(snt)
CATFN(snv)
CATFN(snet)
CATFN(snev)
CATFN(sntv)
CATFN(snetv)
CATFN(sb)
CATFN(sbe)
CATFN(sbt)
CATFN(sbv)
CATFN(sbet)
CATFN(sbev)
CATFN(sbtv)
CATFN(sbetv)

static const struct catfn_tab {
	int flags;
	t_catfn catfn;
} catfn_tabs[] = {
	{ 0,             &catfn_std    },
	{ E,             &catfn_e      },
	{ T,             &catfn_t      },
	{ V,             &catfn_v      },
	{ (E|T),         &catfn_et     },
	{ (E|V),         &catfn_ev     },
	{ (T|V),         &catfn_tv     },
	{ (E|T|V),       &catfn_etv    },
	{ S,             &catfn_s      },
	{ (S|E),         &catfn_se     },
	{ (S|T),         &catfn_st     },
	{ (S|V),         &catfn_sv     },
	{ (S|E|T),       &catfn_set    },
	{ (S|E|V),       &catfn_sev    },
	{ (S|T|V),       &catfn_stv    },
	{ (S|E|T|V),     &catfn_setv   },
	{ N,             &catfn_n      },
	{ (N|E),         &catfn_ne     },
	{ (N|T),         &catfn_nt     },
	{ (N|V),         &catfn_nv     },
	{ (N|E|T),       &catfn_net    },
	{ (N|E|V),       &catfn_nev    },
	{ (N|T|V),       &catfn_ntv    },
	{ (N|E|T|V),     &catfn_netv   },
	{ (N|B),         &catfn_b      },
	{ (N|B|E),       &catfn_be     },
	{ (N|B|T),       &catfn_bt     },
	{ (N|B|V),       &catfn_bv     },
	{ (N|B|E|T),     &catfn_bet    },
	{ (N|B|E|V),     &catfn_bev    },
	{ (N|B|T|V),     &catfn_btv    },
	{ (N|B|E|T|V),   &catfn_betv   },
	{ (S|N),         &catfn_sn     },
	{ (S|N|E),       &catfn_sne    },
	{ (S|N|T),       &catfn_snt    },
	{ (S|N|V),       &catfn_snv    },
	{ (S|N|E|T),     &catfn_snet   },
	{ (S|N|E|V),     &catfn_snev   },
	{ (S|N|T|V),     &catfn_sntv   },
	{ (S|N|E|T|V),   &catfn_snetv  },
	{ (S|N|B),       &catfn_sb     },
	{ (S|N|B|E),     &catfn_sbe    },
	{ (S|N|B|T),     &catfn_sbt    },
	{ (S|N|B|V),     &catfn_sbv    },
	{ (S|N|B|E|T),   &catfn_sbet   },
	{ (S|N|B|E|V),   &catfn_sbev   },
	{ (S|N|B|T|V),   &catfn_sbtv   },
	{ (S|N|B|E|T|V), &catfn_sbetv  }
};

__dead static void
usage(void)
{
	fprintf(stderr,
	    "usage: %s [-AbEeflnsTtuv] [-] [file ...]\n",
	    getprogname());
	exit(EXIT_FAILURE);
}

int
main(int argc, char *argv[])
{
	static char inbuf_static[BUFSIZ], outbuf_static[BUFSIZ];
	static char *fname_stdin = "-";
	int flags, ch, idx, retval, fd;
	const char *fname;
	struct flock l;
	struct stat st;
	size_t i;
	t_catfn catfn;
	long pagesize;

	setprogname((const char *)argv[0]);
	setlocale(LC_ALL, "");

	flags = 0;
	while ((ch = getopt_long(argc, argv,
	    optstr, options, &idx)) != -1) {
		switch (ch) {
		case 'A':
			flags |= (E|T|V);
			break;
		case 'b':
			flags |= (N|B);
			break;
		case 'E':
			flags |= E;
			break;
		case 'e':
			flags |= (E|V);
			break;
		case 'f':
			flags |= F;
			break;
		case 'l':
			l.l_len = 0;
			l.l_start = 0;
			l.l_type = F_WRLCK;
			l.l_whence = SEEK_SET;
			if (fcntl(STDOUT_FILENO, F_SETLKW, &l) == -1)
				err(EXIT_FAILURE, "fcntl");
			break;
		case 'n':
			flags |= N;
			break;
		case 's':
			flags |= S;
			break;
		case 'T':
			flags |= T;
			break;
		case 't':
			flags |= (T|V);
			break;
		case 'u':
			/* ignore -u option */
			break;
		case 'v':
			flags |= V;
			break;
		case '?':
		default:
			usage();
		}
	}
	argc -= optind;
	argv += optind;

	catfn = NULL;
	for (i = 0; i < __arraycount(catfn_tabs); ++i) {
		if (catfn_tabs[i].flags == (flags & ~F)) {
			catfn = catfn_tabs[i].catfn;
			break;
		}
	}
	assert(catfn != NULL);

	if (fstat(STDOUT_FILENO, &st) == 0 &&
	    (size_t)st.st_blksize > sizeof(inbuf_static)) {
		inbufsiz = outbufsiz = st.st_blksize;
		inbuf = malloc((size_t)inbufsiz);
		outbuf = malloc((size_t)outbufsiz);
	}
	if (inbuf == NULL) {
		inbufsiz = sizeof(inbuf_static);
		inbuf = inbuf_static;
	}
	if (outbuf == NULL) {
		outbufsiz = sizeof(outbuf_static);
		outbuf = outbuf_static;
	}
	ohead = outbuf;
	otail = outbuf + outbufsiz;

	if (argc == 0) {
		argc = 1;
		argv = &fname_stdin;
	}
	retval = EXIT_SUCCESS;
	for (i = 0; i < (size_t)argc; ++i) {
		fname = argv[i];
		if (!strcmp(fname, fname_stdin)) {
			fd = STDIN_FILENO;
			fname = "stdin";
		} else if ((fd = open(fname, O_RDONLY)) == -1 || ((flags & F)
		    && (fstat(fd, &st) == -1 || !S_ISREG(st.st_mode)))) {
			warn("%s", fname);
			retval = EXIT_FAILURE;
			continue;
		}
		lnum = 0;
		prev = '\n';
		skip = 0;
		switch ((*catfn)(fd)) {
		case 1:
			err(EXIT_FAILURE, "stdout");
		case 2:
			warn("%s", fname);
			retval = EXIT_FAILURE;
		}
		if (write_std(outbuf, ohead))
			err(EXIT_FAILURE, "stdout");
		if (fd != STDIN_FILENO)
			close(fd);
	}
	exit(retval);
}
