/*-
 * Copyright (c)2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
#if !defined(lint) && !defined(SHELL)
__COPYRIGHT("@(#) Copyright (c) 2021\
 Takehiko NOZAKI, All rights reserved.");
#endif /* not lint */

#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <locale.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *optstr = "123fz";
static const struct option options[] = {
	{ "check-order",      no_argument,       NULL, UCHAR_MAX+1 },
	{ "output-delimiter", required_argument, NULL, UCHAR_MAX+2 },
	{ "total",            no_argument,       NULL, UCHAR_MAX+3 },
	{ "zero-terminated",  no_argument,       NULL, 'z'         },
	{ NULL,               0,                 NULL,  0          },
};

static const char *delim_default = "\t";

struct line {
	char *buf, *prev;
	size_t bufsiz, prevsiz;
};

struct pair {
	struct line line1, line2;
};

struct config {
	bool sup1, sup2, sup3, total;
	int term;
	const char *delim;
	int (*readfn)(struct config *, struct line *, FILE *);
	int (*cmpfn)(const char *, const char *);
	const char *file1, *file2;
};

static inline void
line_init(struct line *l)
{
	l->buf = l->prev = NULL;
}

static inline void
line_uninit(struct line *l)
{
	free(l->buf);
	free(l->prev);
}

static int
line_read_std(struct config *cf, struct line *l, FILE *fp)
{
	size_t buflen;

	buflen = getdelim(&l->buf, &l->bufsiz, cf->term, fp);
	if (buflen == -1) {
		if (ferror(fp))
			err(EXIT_FAILURE, "getdelim");
		return 1;
	}
	if (buflen > 0 && l->buf[buflen - 1] == cf->term)
		l->buf[buflen - 1] = '\0';
	return 0;
}

static int
line_read_check_order(struct config *cf, struct line *l, FILE *fp)
{
	char *tmp;
	size_t tmpsiz;
	ssize_t buflen;

	tmp = l->buf;
	tmpsiz = l->bufsiz;
	l->buf = l->prev;
	l->bufsiz = l->prevsiz;
	l->prev = tmp;
	l->prevsiz = tmpsiz;
	
	if (line_read_std(cf, l, fp))
		return 1;
	if (l->prev && (*cf->cmpfn)(l->prev, l->buf) > 0) {
		if (cf->check)
		abort();
	}
	return 0;
}

static inline void
line_delim(struct config *cf)
{
	if (printf("%s", cf->delim) < 0)
		err(EXIT_FAILURE, "printf");
}

static inline void
line_write(struct config *cf, struct line *l)
{
	if (printf("%s%c", l->buf, cf->term) < 0)
		err(EXIT_FAILURE, "printf");
}

static inline void
pair_init(struct pair *pair)
{
	line_init(&pair->line1);
	line_init(&pair->line2);
}

static inline void
pair_uninit(struct pair *pair)
{
	line_uninit(&pair->line1);
	line_uninit(&pair->line2);
}

static inline void
usage(void)
{
	fprintf(stderr,
"usage: %s [-123fz] file1 file2\n",
	getprogname());
	exit(EXIT_FAILURE);
}

static FILE *
file_open(const char *name, const char *mode)
{
	FILE *fp;

	fp = fopen(name, mode);
	if (fp == NULL)
		err(EXIT_FAILURE, "fopen: %s", name);
	return fp;
}

static void
file_close(FILE *fp)
{
	if (fclose(fp) == EOF)
		errx(EXIT_FAILURE, "fclose");
}

static inline int
config_init(struct config *cf, int argc, char **argv)
{
	int ch, idx;

	cf->readfn = &line_read_std;
	cf->cmpfn = &strcoll;
	cf->term = '\n';
	cf->delim = delim_default;
	cf->file1 = cf->file2 = NULL;
	cf->sup1 = cf->sup2 = cf->sup3 = cf->total = false;

	while ((ch = getopt_long(argc, argv, optstr, options, &idx)) != -1) {
		switch (ch) {
		case '1':
			cf->sup1 = true;
			break;
		case '2':
			cf->sup2 = true;
			break;
		case '3':
			cf->sup3 = true;
			break;
		case 'f':
			cf->cmpfn = &strcasecmp;
			break;
		case 'z':
			cf->term = '\0';
			break;
		case UCHAR_MAX+1:
			cf->readfn = &line_read_check_order;
			break;
		case UCHAR_MAX+2:
			cf->delim = strdup(optarg);
			if (cf->delim == NULL)
				err(EXIT_FAILURE, "strdup");
			break;
		case UCHAR_MAX+3:
			cf->total = true;
			break;
		case '?':
		default:
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	if (argc != 2)
		usage();
	cf->file1 = strdup(argv[0]);
	if (cf->file1 == NULL)
		err(EXIT_FAILURE, "strdup");
	cf->file2 = strdup(argv[1]);
	if (cf->file2 == NULL)
		err(EXIT_FAILURE, "strdup");
	return 0;
}

static void
comm(struct config *cf)
{
	FILE *file1, *file2;
	struct pair pair;
	int cmp, file1eof, file2eof;
	size_t total1, total2, total3;

	file1 = file_open(cf->file1, "r");
	file2 = file_open(cf->file2, "r");
	pair_init(&pair);
	cmp = 0;
	for (;;) {
		if (cmp <= 0)
			file1eof = (*cf->readfn)(cf, &pair.line1, file1);
		if (cmp >= 0)
			file2eof = (*cf->readfn)(cf, &pair.line2, file2);
		if (file1eof) {
			if (!file2eof && (!cf->sup2 || cf->total)) {
				do {
					if (!cf->sup2) {
						if (!cf->sup1)
							line_delim(cf);
						line_write(cf, &pair.line2);
					}
					++total2;
				} while (!(*cf->readfn)(cf, &pair.line2, file2));
			}
			break;
		}
		if (file2eof) {
			if (!file1eof && (!cf->sup1 || cf->total)) {
				do {
					if (!cf->sup1)
						line_write(cf, &pair.line1);
					++total1;
				} while (!(*cf->readfn)(cf, &pair.line1, file1));
			}
			break;
		}
		cmp = (*cf->cmpfn)(pair.line1.buf, pair.line2.buf);
		if (cmp < 0) {
			if (!cf->sup1)
				line_write(cf, &pair.line1);
			++total1;
		} else if (cmp > 0) {
			if (!cf->sup2) {
				if (!cf->sup1)
					line_delim(cf);
				line_write(cf, &pair.line2);
			}
			++total2;
		} else {
			if (!cf->sup3) {
				if (!cf->sup1)
					line_delim(cf);
				if (!cf->sup2)
					line_delim(cf);
				line_write(cf, &pair.line2);
			}
			++total3;
		}
	}
	if (cf->total)
		printf("%1$zd%4$s%2$zd%4$s%3$zd%4$stotal%5$c",
		    total1, total2, total3, cf->delim, cf->term);
	pair_uninit(&pair);
	file_close(file1);
	file_close(file2);
}

static inline void
config_uninit(struct config *cf)
{
	if (cf->delim != delim_default)
		free(__UNCONST(cf->delim));
	free(__UNCONST(cf->file1));
	free(__UNCONST(cf->file2));
}

int
main(int argc, char *argv[])
{
	struct config cf;

	setprogname((const char *)argv[0]);

	setlocale(LC_ALL, "");

	if (config_init(&cf, argc, argv))
		usage();
	comm(&cf);
	config_uninit(&cf);

	exit(EXIT_SUCCESS);
}
